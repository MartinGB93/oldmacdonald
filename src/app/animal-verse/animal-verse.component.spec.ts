import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimalVerseComponent } from './animal-verse.component';
import {FormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';

describe('AnimalVerseComponent', () => {
  let component: AnimalVerseComponent;
  let fixture: ComponentFixture<AnimalVerseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimalVerseComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimalVerseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
