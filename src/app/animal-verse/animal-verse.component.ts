import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-animal-verse',
  templateUrl: './animal-verse.component.html',
  styleUrls: ['./animal-verse.component.scss']
})
export class AnimalVerseComponent implements OnInit {

  @Input() animal;

  constructor() { }

  ngOnInit() {
  }

}
