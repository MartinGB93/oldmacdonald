import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AutoComponent } from './auto/auto.component';
import { ManualComponent } from './manual/manual.component';
import { FormsModule } from '@angular/forms';
import { AnimalVerseComponent } from './animal-verse/animal-verse.component';

@NgModule({
  declarations: [
    AppComponent,
    AutoComponent,
    ManualComponent,
    AnimalVerseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
