import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoComponent } from './auto.component';
import {FormsModule} from '@angular/forms';
import {AnimalVerseComponent} from '../animal-verse/animal-verse.component';

describe('AutoComponent', () => {
  let component: AutoComponent;
  let fixture: ComponentFixture<AutoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AutoComponent,
        AnimalVerseComponent
      ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
