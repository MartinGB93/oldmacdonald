import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auto',
  templateUrl: './auto.component.html',
  styleUrls: ['./auto.component.scss']
})
export class AutoComponent implements OnInit {

  farmAnimals = [
    { name: 'dog',
      sound: 'woof',
      icon: '🐕',
    },
    { name: 'cat',
      sound: 'meow',
      icon: '🐈'
    },
    { name: 'cow',
      sound: 'moo',
      icon: '🐄'
    },
    { name: 'rooster',
      sound: 'cock-a-doodle-doo',
      icon: '🐓'
    },
    { name: 'horse',
      sound: 'ree',
      icon: '🐎'
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
