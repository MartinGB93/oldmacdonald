import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualComponent } from './manual.component';
import {By} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {AnimalVerseComponent} from '../animal-verse/animal-verse.component';

describe('ManualComponent', () => {
  let component: ManualComponent;
  let fixture: ComponentFixture<ManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ManualComponent,
        AnimalVerseComponent
      ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualComponent);
    component = fixture.componentInstance;
    component.songCreated = true;
    component.animalName = undefined;
    component.animalSound = undefined;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not display the create button if inputs are empty', () => {
    fixture.detectChanges();
    const animalInput = fixture.debugElement.query(By.css('.animalInput')).nativeElement;
    const animalSoundInput = fixture.debugElement.query(By.css('.animalSoundInput')).nativeElement;
    const createSongButton = fixture.debugElement.query(By.css('.createSongButton'));
    if (animalInput.value && animalSoundInput.value) {
      expect(createSongButton).not.toBeNull();
    } else {
      expect(createSongButton).toBeNull();
    }
  });

  it('should not display the verse if not created', () => {
    fixture.detectChanges();
    const verse = fixture.debugElement.query(By.css('.animalSong'));
    expect(verse).not.toBeNull();
  });
});
