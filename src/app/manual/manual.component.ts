import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-manual',
  templateUrl: './manual.component.html',
  styleUrls: ['./manual.component.scss']
})
export class ManualComponent implements OnInit {

  animalName;
  animalSound;
  animalObject = {};
  songCreated = false;

  constructor() { }

  ngOnInit() {
  }

  createSong() {
    this.songCreated = true;
    this.animalObject = {
      name: this.animalName,
      sound: this.animalSound
    };
  }
}
