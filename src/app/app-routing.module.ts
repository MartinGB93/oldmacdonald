import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AutoComponent} from 'src/app/auto/auto.component'
import {ManualComponent} from 'src/app/manual/manual.component'

const routes: Routes = [
  {
    path: 'auto',
    component: AutoComponent,
  },
  {
    path: 'manual',
    component: ManualComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
